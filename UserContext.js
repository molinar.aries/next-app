import React from 'react';

//create a context object
const UserContext = React.createContext();

//Provider component that allows consuming to subcribe to context changes
export const UserProvider = UserContext.Provider;

export default UserContext;